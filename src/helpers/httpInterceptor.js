import axios from 'axios';

export default function execute() {
    axios.interceptors.request.use(function(config) {
        const token = 'thisIsMyTokenForExample';
        if(token) {
            config.headers.Authorization = `Bearer ${token}`;
            console.log(config);
            return config;
        } else {
            console.log('There is not token yet...');
            return config;
        }
    }, function(err) {
        return Promise.reject(err);
    });
}